(function(window) {
  let sliders = qsAll('.slider');
  

  sliders.forEach(sliderNode => {
    let cards = $(sliderNode).find('.card');
    let isNews = $(sliderNode).find('.card--small').length > 1;
    let heights = [];
    let slidesAmount = Math.round(cards.length / 2) - 1;
    
    if (isNews) {
      let slidesAmount = Math.round(cards.length / 3);
    }

    sliderNode.classList.add('at-start');

    if (window.innerWidth > 520 && cards.length > 3 || window.innerWidth <= 520 && cards.length > 1) {

      $(sliderNode).append(`
        <button class="next"></button>
        <button class="prev"></button>
      `);

      const next = qs('.next', sliderNode);
      const prev = qs('.prev', sliderNode);

      cards.wrap(`<div class="slide"></div>`).parent();
  
      let slider = new Flickity(sliderNode, {
        cellAlign:       'left',
        draggable:       true,
        freeScroll:      false,
        wrapAround:      false,
        groupCells:      true,
        cellSelector:    '.slide',
        accesibility:    true,
        adaptiveHeight:  false,
        prevNextButtons: false,
        pageDots:        window.innerWidth <= 520,
        on: {
          ready: () => {
            cards.each(function() {
              heights.push($(this).height());
            });
        
            let maxHeight = Math.max.apply(null, heights);
        
            cards.each(function() {
              if (window.innerWidth < 520) {
                $(this).height(maxHeight + 20);
              }

              else {
                $(this).height(maxHeight);
              }
            });

            $(sliderNode).parent().addClass('section--collapsed')
            
            if (window.innerWidth < 520) {
              setTimeout(() => {
                let sliderHeight = $(sliderNode).height();
                $(sliderNode).height(sliderHeight + 20);
              }, 500);
            }

            // Настройка слайдов на десктопе
            if (window.innerWidth > 520) {
              window.setupSections();

              setTimeout(() => {
                window.setupSections();
              }, 500);
            }
          }
        }
      });

      slider.on('change', function(index) {
        console.log(index, slider.slides.length - 1, isNews ? 'news' : 'articles');
        console.log(slider);

        if (index === 0) {
          sliderNode.classList.add('at-start')
        } else {
          sliderNode.classList.remove('at-start')
        }

        if (index === slider.slides.length - 1) {
          sliderNode.classList.add('at-end')
        } else {
          sliderNode.classList.remove('at-end')
        }
      });

      slider.on('dragMove', function( event, pointer, moveVector ) {
        if (moveVector.x > 0) {
          prev.classList.add('active');
        } else if (moveVector.x < 0) {
          next.classList.add('active');
        }
      });

      slider.on('dragEnd', function( event, pointer ) {
        next.classList.remove('active');
        prev.classList.remove('active');
      });

      next.addEventListener('click', () => {
        slider.next();
      });

      prev.addEventListener('click', () => {
        slider.previous();
      });
    }
  });
})(window);
