(function(window) {
  if (window.innerWidth > 520) {
    const mainContainer = qs('.body');
    const sections = qsAll('.month', mainContainer);
    const headerTitle = qs('.header h1');

    let bodyHeight = window.innerHeight;
    
    function setupSections() {
      let zIndex = 500;

      bodyHeight = sections.reduce((prev, section) => {
        section.style.zIndex = zIndex--;
        section.dataset.offset = prev;

        return prev + $(section).height();
      }, 0);

      mainContainer.style.height = `${bodyHeight}px`;
    }
    window.setupSections = setupSections;

    let S, Sh = 0;
    let currentMonth = '';
    let firstLoad = true;

    function setHeaderMonth(month) {
      if (currentMonth !== month) {
        headerTitle.textContent = month;
      }

      currentMonth = month;
    }
    
    function checkScroll() {
      S  = window.scrollY;
      Sh = window.scrollY + window.innerHeight;
      
      sections.forEach(section => {
        let topOffset = parseInt( section.dataset.offset );
        let bottomOffset = topOffset + $(section).height();
        let month = section.dataset.month;

        let cond1 = S >= topOffset;
        let cond2 = cond1 && Sh < bottomOffset + window.innerHeight;

        if (cond1 || cond2) {

          section.classList.add('current');
          
          TweenMax.to(section, 0.3, {
            y: (S - topOffset) * -1
          });
          
          const nextMonthNode = section.nextSibling.nextSibling;
          if (nextMonthNode) {
            TweenMax.to(nextMonthNode, 0.3, {y: 0});
          }
          
          setHeaderMonth(month);
        } else {
          section.classList.remove('current');
        }
      });
    }
    
    checkScroll();
    setupSections();
    window.addEventListener('scroll', e => {
      window.requestAnimationFrame(checkScroll);
    });
  }

})(window); 
