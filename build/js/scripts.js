'use strict';

(function (window) {

  var header = qs('.header');
  var scrollLimit = window.innerHeight / 2;

  window.addEventListener('resize', function (e) {
    scrollLimit = window.innerHeight / 2;
  });

  window.addEventListener('scroll', function (e) {
    var scrolled = window.scrollY;

    if (scrolled > scrollLimit) {
      header.classList.remove('hidden');
    } else {
      header.classList.add('hidden');
    }
  });
})(window);
"use strict";

function qs(query) {
  var container = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

  return container.querySelector(query);
}

function qsAll(query) {
  var container = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

  return [].slice.call(container.querySelectorAll(query));
}
// (function() {
//   const ill = qs('#illustration');

//   ill.addEventListener('load', e => {
//     console.log(e);
//     setupSections();
//   })
// })()
"use strict";
'use strict';

(function (window) {
  if (window.innerWidth > 520) {
    var setupSections = function setupSections() {
      var zIndex = 500;

      bodyHeight = sections.reduce(function (prev, section) {
        section.style.zIndex = zIndex--;
        section.dataset.offset = prev;

        return prev + $(section).height();
      }, 0);

      mainContainer.style.height = bodyHeight + 'px';
    };

    var setHeaderMonth = function setHeaderMonth(month) {
      if (currentMonth !== month) {
        headerTitle.textContent = month;
      }

      currentMonth = month;
    };

    var checkScroll = function checkScroll() {
      S = window.scrollY;
      Sh = window.scrollY + window.innerHeight;

      sections.forEach(function (section) {
        var topOffset = parseInt(section.dataset.offset);
        var bottomOffset = topOffset + $(section).height();
        var month = section.dataset.month;

        var cond1 = S >= topOffset;
        var cond2 = cond1 && Sh < bottomOffset + window.innerHeight;

        if (cond1 || cond2) {

          section.classList.add('current');

          TweenMax.to(section, 0.3, {
            y: (S - topOffset) * -1
          });

          var nextMonthNode = section.nextSibling.nextSibling;
          if (nextMonthNode) {
            TweenMax.to(nextMonthNode, 0.3, { y: 0 });
          }

          setHeaderMonth(month);
        } else {
          section.classList.remove('current');
        }
      });
    };

    var mainContainer = qs('.body');
    var sections = qsAll('.month', mainContainer);
    var headerTitle = qs('.header h1');

    var bodyHeight = window.innerHeight;

    window.setupSections = setupSections;

    var S = void 0,
        Sh = 0;
    var currentMonth = '';
    var firstLoad = true;

    checkScroll();
    setupSections();
    window.addEventListener('scroll', function (e) {
      window.requestAnimationFrame(checkScroll);
    });
  }
})(window);
'use strict';

(function (window) {
  var sliders = qsAll('.slider--media');

  sliders.forEach(function (sliderNode) {
    $(sliderNode).append('\n      <button class="next"></button>\n      <button class="prev"></button>\n    ');

    sliderNode.classList.add('at-start');

    var slidesAmount = $(sliderNode).find('.slide').length - 1;

    var next = qs('.next', sliderNode);
    var prev = qs('.prev', sliderNode);

    var slider = new Flickity(sliderNode, {
      cellAlign: 'center',
      draggable: true,
      freeScroll: false,
      wrapAround: false,
      cellSelector: '.slide',
      accesibility: true,
      adaptiveHeight: false,
      prevNextButtons: false,
      pageDots: true
    });

    slider.on('change', function (index) {
      if (index === 0) {
        sliderNode.classList.add('at-start');
      } else {
        sliderNode.classList.remove('at-start');
      }

      if (index === slidesAmount) {
        sliderNode.classList.add('at-end');
      } else {
        sliderNode.classList.remove('at-end');
      }
    });

    slider.on('dragMove', function (event, pointer, moveVector) {
      if (moveVector.x > 0) {
        prev.classList.add('active');
      } else if (moveVector.x < 0) {
        next.classList.add('active');
      }
    });

    slider.on('dragEnd', function (event, pointer) {
      next.classList.remove('active');
      prev.classList.remove('active');
    });

    next.addEventListener('click', function () {
      slider.next();
    });

    prev.addEventListener('click', function () {
      slider.previous();
    });
  });
})(window);
'use strict';

(function (window) {
  var sliders = qsAll('.slider');

  sliders.forEach(function (sliderNode) {
    var cards = $(sliderNode).find('.card');
    var isNews = $(sliderNode).find('.card--small').length > 1;
    var heights = [];
    var slidesAmount = Math.round(cards.length / 2) - 1;

    if (isNews) {
      var _slidesAmount = Math.round(cards.length / 3);
    }

    sliderNode.classList.add('at-start');

    if (window.innerWidth > 520 && cards.length > 3 || window.innerWidth <= 520 && cards.length > 1) {

      $(sliderNode).append('\n        <button class="next"></button>\n        <button class="prev"></button>\n      ');

      var next = qs('.next', sliderNode);
      var prev = qs('.prev', sliderNode);

      cards.wrap('<div class="slide"></div>').parent();

      var slider = new Flickity(sliderNode, {
        cellAlign: 'left',
        draggable: true,
        freeScroll: false,
        wrapAround: false,
        groupCells: true,
        cellSelector: '.slide',
        accesibility: true,
        adaptiveHeight: false,
        prevNextButtons: false,
        pageDots: window.innerWidth <= 520,
        on: {
          ready: function ready() {
            cards.each(function () {
              heights.push($(this).height());
            });

            var maxHeight = Math.max.apply(null, heights);

            cards.each(function () {
              if (window.innerWidth < 520) {
                $(this).height(maxHeight + 20);
              } else {
                $(this).height(maxHeight);
              }
            });

            $(sliderNode).parent().addClass('section--collapsed');

            if (window.innerWidth < 520) {
              setTimeout(function () {
                var sliderHeight = $(sliderNode).height();
                $(sliderNode).height(sliderHeight + 20);
              }, 500);
            }

            // Настройка слайдов на десктопе
            if (window.innerWidth > 520) {
              window.setupSections();

              setTimeout(function () {
                window.setupSections();
              }, 500);
            }
          }
        }
      });

      slider.on('change', function (index) {
        console.log(index, slider.slides.length - 1, isNews ? 'news' : 'articles');
        console.log(slider);

        if (index === 0) {
          sliderNode.classList.add('at-start');
        } else {
          sliderNode.classList.remove('at-start');
        }

        if (index === slider.slides.length - 1) {
          sliderNode.classList.add('at-end');
        } else {
          sliderNode.classList.remove('at-end');
        }
      });

      slider.on('dragMove', function (event, pointer, moveVector) {
        if (moveVector.x > 0) {
          prev.classList.add('active');
        } else if (moveVector.x < 0) {
          next.classList.add('active');
        }
      });

      slider.on('dragEnd', function (event, pointer) {
        next.classList.remove('active');
        prev.classList.remove('active');
      });

      next.addEventListener('click', function () {
        slider.next();
      });

      prev.addEventListener('click', function () {
        slider.previous();
      });
    }
  });
})(window);